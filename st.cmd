# -----------------------------------------------------------------------------
# LEBT Chopper - Test Bench
# -----------------------------------------------------------------------------
# XT Pico 
# -----------------------------------------------------------------------------
require(xtpico,0.9.0)
require(common, 0.3.0)

# Prefix for all records
epicsEnvSet("PREFIX",           "UTG-XTPICO-01")
epicsEnvSet("DEVICE_IP",        "192.168.100.100")

epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")
epicsEnvSet("I2C_TMP100_PORT",  "AK_I2C_TMP100")
epicsEnvSet("I2C_LTC2991_PORT", "AK_I2C_LTC2991")
epicsEnvSet("I2C_ADT7420_PORT", "AK_I2C_ADT7420")

# -----------------------------------------------------------------------------
#- Create the asyn port to talk XTpico server on TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")

# -----------------------------------------------------------------------------
# Debug
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

# -----------------------------------------------------------------------------
# PMT temperature sensor
# -----------------------------------------------------------------------------
#iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:I2C1:Temp1:, COUNT=1, INFOS=0x49")
#AKI2CTMP100Configure("TMP100.1", "AK_I2C_COMM", 1, "0x49", 1, 0, 0)
#dbLoadRecords("AKI2C_TMP100.db", "P=UTG-XTPICO-01, R=:I2C1:Temp1:, PORT=TMP100.1, IP_PORT=AK_I2C_COMM, ADDR=0, TIMEOUT=1")

AKI2CADT7420Configure($(I2C_ADT7420_PORT), $(I2C_COMM_PORT), 1, "0x49", 1, 0, 0)
dbLoadRecords("AKI2C_ADT7420.db", "P=$(PREFIX),R=:I2C1:Temp1:,PORT=$(I2C_ADT7420_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# Voltage and current monitor
# -----------------------------------------------------------------------------
#iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=2, NAME=:I2C1:VMon1:, COUNT=1, INFOS=0x90")
AKI2CLTC2991Configure($(I2C_LTC2991_PORT), $(I2C_COMM_PORT), 1, "0x48", 0, 0, 1, 0, 0)
dbLoadRecords("AKI2C_LTC2991.db", "P=UTG-XTPICO-01,R=:I2C1:VMon1:,PORT=$(I2C_LTC2991_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# Loading COMMON modules
iocshLoad("$(common_DIR)e3-common.iocsh")

iocInit()
